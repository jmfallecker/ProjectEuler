#include "stdafx.h"
#include <math.h>
#include <vector>
#include <limits.h>

using namespace std;

///Generates prime lower than the passed in param:number
vector<bool> generatePrimesUpTo(long long number)
{
	vector<bool> primes(number, true);

	bool hasCurrentNumberChanged = true;
	long long currentNumber = 2;

	// while we are still looking at multiples of the same number
	while (hasCurrentNumberChanged)
	{
		// starting with the first multiple of our current number, mark all mutiples in the list as "not-prime"
		for (long long i = currentNumber * 2; i < primes.size(); i += currentNumber)
		{
			primes[i] = false;
		}

		// starting with the number after the current number
		// find the next number that has not been marked "non-prime"
		for (long long i = currentNumber + 1; i < primes.size(); ++i)
		{
			long long previousNumber = currentNumber;

			if (primes[i])
			{
				currentNumber = i;
				hasCurrentNumberChanged = true;
				break;
			}

			hasCurrentNumberChanged = false;
		}
	}

	return primes;
}

///Generates primes lower than the passed in param:number
vector<long long> generatePrimes(long long number)
{
	vector<bool> primesMarked = generatePrimesUpTo(number);

	long numberOfPrimes = 0;
	for (long i = 0; i < primesMarked.size(); ++i)
	{
		if (primesMarked[i])
			++numberOfPrimes;
	}

	vector<long long> primes(numberOfPrimes, 0);

	for (long long i = 0; i < numberOfPrimes; ++i)
	{
		if (primesMarked[i])
			primes.push_back(i);
	}

	return primes;
}

///Creates vector of an amount of booleans (with indices marked as "true" being prime) equal to param:number
vector<bool> generateAmountOfIsPrimes(long long number)
{
	vector<bool> primes(1000000, true);

	bool hasCurrentNumberChanged = true;
	long long currentNumber = 2;
	long long count = 1;

	// while we are still looking at multiples of the same number
	while (hasCurrentNumberChanged && count != number)
	{
		// starting with the first multiple of our current number, mark all mutiples in the list as "not-prime"
		for (long long i = currentNumber * 2; i < primes.size(); i += currentNumber)
		{
			primes[i] = false;
		}

		// starting with the number after the current number
		// find the next number that has not been marked "non-prime"
		for (long long i = currentNumber + 1; i < primes.size(); ++i)
		{
			long long previousNumber = currentNumber;

			if (primes[i])
			{
				currentNumber = i;
				hasCurrentNumberChanged = true;
				++count;
				break;
			}

			hasCurrentNumberChanged = false;
		}
	}

	return primes;


}

///Generates an amount of primes equal to the passed in param:number
vector<long long> generateAmountOfPrimes(long long number)
{
	vector<bool> primesMarked = generateAmountOfIsPrimes(number);
	vector<long long> primes(0, 0);

	for (long long i = 2; i < primesMarked.size(); ++i)
	{
		if (primesMarked[i])
			primes.push_back(i);
	}

	return primes;
}

///Returns whether passed in value is a palindrome (same forwards and backwards)
bool isPalindromic(string value)
{
	bool isPalindromic = true;
	int j = value.length() - 1;

	for (int i = 0; i < value.length(); ++i, --j)
	{
		if (value[i] != value[j])
			isPalindromic = false;
	}

	return isPalindromic;
}

///Converts a character c to an integer value
int charToInteger(char c)
{
	return c - 48;
}