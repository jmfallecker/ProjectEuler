#include "stdafx.h"

int Problem5()
{
	bool hasNumberBeenFound = false;
	int solution = -1;

	for (int i = 20; !hasNumberBeenFound; i += 20)
	{
		bool isDivisible = true;
		for (int j = 19; j > 0; --j)
		{
			if (i % j != 0)
			{
				isDivisible = false;
				break;
			}
		}

		if (isDivisible)
		{
			solution = i;
			break;
		}			
	}
	
	return solution;
}