#include "stdafx.h"

bool isDivisibleBy(long input, int modulus)
{
	return input % modulus == 0;
}

int Problem1()
{
	const int ONE_THOUSAND = 1000;
	int sum = 0;

	for (int i = 0; i < ONE_THOUSAND; ++i)
	{
		if (isDivisibleBy(i, 3) || isDivisibleBy(i, 5))
		{
			sum += i;
		}
	}

	return sum;
}