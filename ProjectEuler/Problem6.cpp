#include "stdafx.h"

int Problem6()
{
	long long squareOfSum = 0;
	long long sumOfSquares = 0;
	
	for (int i = 1; i <= 100; ++i)
	{
		sumOfSquares += (i * i);
		squareOfSum += i;
	}

	squareOfSum = squareOfSum * squareOfSum;

	return squareOfSum - sumOfSquares;
}