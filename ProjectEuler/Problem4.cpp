#include "stdafx.h"
#include <string>

bool isPalindromic(std::string value);

int Problem4()
{
	int greatestPalindrome = -1;

	for (int i = 999; i > 0; --i)
	{
		for (int j = 999; j > 0; --j)
		{
			int product = i * j;
			if (isPalindromic(std::to_string(product)) && product > greatestPalindrome)
			{
				greatestPalindrome = product;
				break;
			}
			else if (product < greatestPalindrome)
				break;
		}
	}

	return greatestPalindrome;
}