#include "stdafx.h"

bool isDivisibleBy(long input, int modulus);

int generateNextFibonacci(int fib1, int fib2)
{
	return fib1 + fib2;
}

int Problem2()
{
	const int FOUR_MILLION = 4 * 1000 * 1000;

	int fib1 = 1;
	int fib2 = 2;
	int sum = 0;

	while (generateNextFibonacci(fib1, fib2) < FOUR_MILLION)
	{
		if (isDivisibleBy(fib2, 2))
			sum += fib2;

		int temp = fib1;
		fib1 = fib2;
		fib2 = generateNextFibonacci(temp, fib2);
	}

	if (isDivisibleBy(fib2, 2))
		sum += fib2;

	return sum;
}