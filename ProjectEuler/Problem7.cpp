#include "stdafx.h"
#include "Primes.h"
#include <vector>

long long Problem7()
{
	std::vector<long long> primes = generateAmountOfPrimes(10001);
	long long value = primes[10000];
	return value;
}