#include "stdafx.h"
#include "Primes.h"
#include <vector>
#include <math.h>


long long Problem3()
{
	const unsigned long long NUMBER = 600851475143LL;

	std::vector<long long> primes = generatePrimes(ceil(sqrt(NUMBER)));
	long long gpf = -1;

	for (long long i = primes.size() - 1; i > 0; --i)
	{
		if (NUMBER % primes[i] == 0)
		{
			gpf = primes[i];
			break;
		}
	}

	return gpf;
}